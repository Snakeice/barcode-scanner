unit BarcodeScanner.Component;

interface

uses
  System.SysUtils, providers.intent.Barcode, System.Classes;

type
  TOnReadCode = procedure(ACode: string) of Object;
  TBarcodeScanner = class(TComponent)
  private
    FHandler: TOnReadCode;
    FActive: Boolean;
    function GetOnCodeRead: TOnReadCode;
    procedure SetOnCodeRead(const Value: TOnReadCode);
    procedure SetActive(const Value: Boolean);

  public
    Procedure Pause;
    Procedure Resume;
    function Model: TScannerTypes;
  published
    property Active: Boolean read FActive write SetActive;
    property OnCodeRead: TOnReadCode read GetOnCodeRead write SetOnCodeRead;
  end;

  procedure Register;

implementation


{ TBarcodeScanner }

function TBarcodeScanner.GetOnCodeRead: TOnReadCode;
begin
 Result := FHandler;
end;

function TBarcodeScanner.Model: TScannerTypes;
begin
  Result := TProviderBarcode.Model;
end;

procedure TBarcodeScanner.Pause;
begin
 TProviderBarcode.SetHandler(nil);
end;

procedure TBarcodeScanner.Resume;
begin
 TProviderBarcode.SetHandler(FHandler);
end;

procedure TBarcodeScanner.SetActive(const Value: Boolean);
begin
  FActive := Value;
  if FActive then
    Resume
  else
    Pause;
end;

procedure TBarcodeScanner.SetOnCodeRead(const Value: TOnReadCode);
begin
  TProviderBarcode.SetHandler(Value);
  FHandler := Value;
end;

procedure Register;
begin
  RegisterComponents('HashLoad', [TBarcodeScanner]);
end;


end.
