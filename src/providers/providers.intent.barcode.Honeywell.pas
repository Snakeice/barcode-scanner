unit providers.intent.barcode.Honeywell;

interface

{$IFDEF ANDROID}
uses
  FMX.Platform, System.Messaging, Androidapi.JNI.GraphicsContentViewText,
  System.SysUtils, Androidapi.JNIBridge, Androidapi.JNI.Embarcadero;

type

  TReceiver = class(TJavaLocal, JFMXBroadcastReceiverListener)
  public
    FHandler: TProc<string>;
    constructor Create(AHandler: TProc<string>);
    procedure onReceive(context: JContext; intent: JIntent); cdecl;
  end;

  TProviderBarcodeHoneywell = class
  private
    FHandler: TProc<string>;
    FPausedHandler: TProc;
    FListener: TReceiver;
    FBroadcastReceiver: JFMXBroadcastReceiver;
    FPaused: Boolean;
    constructor Create();
    procedure RegisterReciver;
    procedure UnregisterReciver;

  protected
    destructor Destroy; override;
    procedure HandleLocal(AData: string);
  public
    procedure SetHandler(AHandler: TProc<string>);
    procedure SetPausedHandler(AHandler: TProc);
    procedure Pause;
    procedure Resume;
    function GetHandler: TProc<String>;

    class function GetInstance: TProviderBarcodeHoneywell;
  end;

{$ENDIF}
implementation

{$IFDEF ANDROID}

uses FMX.Platform.Android, Androidapi.JNI.JavaTypes, Androidapi.JNI.Net,
  Androidapi.JNI.Os, Androidapi.Helpers, fmx.forms;

var
  _Instance: TProviderBarcodeHoneywell;

{ TProviderBarcodeHoneywell }

constructor TProviderBarcodeHoneywell.Create;
begin
  FListener := TReceiver.Create(HandleLocal);
  FBroadcastReceiver := TJFMXBroadcastReceiver.JavaClass.init(FListener);
  FPaused := False;
end;

destructor TProviderBarcodeHoneywell.Destroy;
begin
  UnregisterReciver;
  inherited;
end;

class function TProviderBarcodeHoneywell.GetInstance: TProviderBarcodeHoneywell;
begin
  if not Assigned(_Instance) then
    _Instance := TProviderBarcodeHoneywell.Create;

  Result := _Instance;
end;

procedure TProviderBarcodeHoneywell.HandleLocal(AData: string);
begin
  if not(FPaused) and Assigned(FHandler) then
  begin
    FHandler(AData)
  end
  else
  begin
    if Assigned(FPausedHandler) then
      FPausedHandler();
  end;
end;

procedure TProviderBarcodeHoneywell.Pause;
begin
  FPaused := True;
end;

procedure TProviderBarcodeHoneywell.RegisterReciver;
var
  Filter: JIntentFilter;
begin
  Filter := TJIntentFilter.JavaClass.init(StringToJString('android.intent.action.BARCODE'));
  TAndroidHelper.context.getApplicationContext.registerReceiver(FBroadcastReceiver, Filter);
end;

procedure TProviderBarcodeHoneywell.Resume;
begin
  FPaused := False;
end;


function TProviderBarcodeHoneywell.GetHandler: TProc<String>;
begin
  Result := FHandler;
end;

procedure TProviderBarcodeHoneywell.SetHandler(AHandler: TProc<string>);
begin
  FHandler := AHandler;
  if Assigned(AHandler) then
    RegisterReciver
  else
    UnregisterReciver;
end;

procedure TProviderBarcodeHoneywell.SetPausedHandler(AHandler: TProc);
begin
  FPausedHandler := AHandler;
end;

procedure TProviderBarcodeHoneywell.UnregisterReciver;
begin
  TAndroidHelper.context.getApplicationContext.unregisterReceiver(FBroadcastReceiver);
end;

{ TReceiver }

constructor TReceiver.Create(AHandler: TProc<string>);
begin
  inherited Create();
  FHandler := AHandler;
end;

procedure TReceiver.onReceive(context: JContext; intent: JIntent);
var
  LData: string;
begin
  LData := JStringToString(intent.getExtras.getString(StringToJString('data'))).Trim;
  if not LData.IsEmpty then
    FHandler(LData);
end;


initialization
//  TProviderBarcodeHonneywell.GetInstance;
finalization
  if Assigned(_Instance) then
    _Instance.DisposeOf;

{$ENDIF}
end.

