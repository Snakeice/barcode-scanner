unit providers.intent.barcode.Zebra;

interface
{$IFDEF ANDROID}

uses
  FMX.Platform, System.Messaging, Androidapi.JNI.GraphicsContentViewText,
  System.SysUtils;

type
  TProviderBarcodeZebra = class
  private
    FHandler: TProc<string>;
    FPausedHandler: TProc;
    FPaused: Boolean;
    constructor Create();

    function HandleAppEvent(AAppEvent: TApplicationEvent; AContext: TObject): Boolean;
    procedure HandleActivityMessage(const Sender: TObject; const M: TMessage);
    function HandleIntentAction(const Data: JIntent): Boolean;

  public
    procedure SetHandler(AHandler: TProc<string>);
    procedure SetPausedHandler(AHandler: TProc);
    procedure Pause;
    procedure Resume;
    function GetHandler: TProc<String>;

    class function GetInstance: TProviderBarcodeZebra;
  end;

{$ENDIF}

implementation

{$IFDEF ANDROID}

uses FMX.Platform.Android, Androidapi.JNI.JavaTypes, Androidapi.JNI.Net,
  Androidapi.JNI.Os, Androidapi.Helpers, fmx.forms;

var
  _Instance: TProviderBarcodeZebra;

{ TProviderBarcodeZebra }

constructor TProviderBarcodeZebra.Create;
var
  LAppEventService: IFMXApplicationEventService;
begin
  FPaused := False;
  if TPlatformServices
       .Current
       .SupportsPlatformService(IFMXApplicationEventService, LAppEventService) then
    LAppEventService.SetApplicationEventHandler(HandleAppEvent);


  MainActivity
    .registerIntentAction(StringToJString('android.intent.action.BARCODE'));

  TMessageManager
    .DefaultManager
    .SubscribeToMessage(TMessageReceivedNotification, HandleActivityMessage)
end;

class function TProviderBarcodeZebra.GetInstance: TProviderBarcodeZebra;
begin
  if not Assigned(_Instance) then
    _Instance := TProviderBarcodeZebra.Create;

  Result := _Instance;
end;

procedure TProviderBarcodeZebra.HandleActivityMessage(const Sender: TObject;
  const M: TMessage);
begin
  if M is TMessageReceivedNotification then
    HandleIntentAction(TMessageReceivedNotification(M).Value);
end;

function TProviderBarcodeZebra.HandleAppEvent(AAppEvent: TApplicationEvent;
  AContext: TObject): Boolean;
var
  LStartupIntent: JIntent;
begin
  Result := False;
  if AAppEvent = TApplicationEvent.BecameActive then
  begin
    LStartupIntent := MainActivity.getIntent;
    if LStartupIntent <> nil then
      HandleIntentAction(LStartupIntent);
  end;
end;

function TProviderBarcodeZebra.HandleIntentAction(const Data: JIntent): Boolean;
var
  Extras: JBundle;
  LData: string;
begin
  Result := False;
  if Data <> nil then
  begin
    Extras := Data.getExtras;
    if Extras <> nil then
    begin
      LData := JStringToString
        (Extras.getString(StringToJString('com.symbol.datawedge.data_string')));

      if not(FPaused) and Assigned(FHandler) then
      begin
        if not LData.IsEmpty then
          FHandler(LData)
      end
      else
      begin
        if Assigned(FPausedHandler) then
          FPausedHandler();
      end;
    end;
  end;
end;

procedure TProviderBarcodeZebra.Pause;
begin
  FPaused := True;
end;

procedure TProviderBarcodeZebra.Resume;
begin
  FPaused := False;
end;

function TProviderBarcodeZebra.GetHandler: TProc<String>;
begin
  Result := FHandler;
end;

procedure TProviderBarcodeZebra.SetHandler(AHandler: TProc<string>);
begin
  FHandler := AHandler;
end;

procedure TProviderBarcodeZebra.SetPausedHandler(AHandler: TProc);
begin
  FPausedHandler := AHandler;
end;

initialization
finalization
  if Assigned(_Instance) then
    _Instance.DisposeOf;


{$ENDIF}
end.
