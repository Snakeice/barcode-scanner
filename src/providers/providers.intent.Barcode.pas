unit providers.intent.Barcode;

interface

uses
  FMX.Platform, System.SysUtils;

type
  TScannerTypes = (scHoneywell, scZebra);


  TProviderBarcode = class
  public
    class procedure SetHandler(AHandler: TProc<string>);
    class function GetHandler: TProc<string>;
    class procedure SetPausedCallback(AHandler: TProc);
    class procedure Resume;
    class procedure Pause;
    class var Model: TScannerTypes;
  end;

const
  TScannerTypesString: array [TScannerTypes] of string = ('Honeywell', 'Zebra');
implementation

{$IFDEF ANDROID}
uses providers.intent.Barcode.Honeywell, providers.intent.Barcode.Zebra,
  androidapi.JNI.JavaTypes,
  Androidapi.Helpers,
  androidapi.JNI.Os;
{$ENDIF}

{ TProviderBarcode }

class procedure TProviderBarcode.Pause;
begin
{$IFDEF ANDROID}
  case TProviderBarcode.Model of
    scHoneywell: TProviderBarcodeHoneywell.GetInstance.Pause;
    scZebra    : TProviderBarcodeZebra.GetInstance.Pause;
  end;
{$ENDIF}
end;

class procedure TProviderBarcode.Resume;
begin
{$IFDEF ANDROID}
  case TProviderBarcode.Model of
    scHoneywell: TProviderBarcodeHoneywell.GetInstance.Resume;
    scZebra    : TProviderBarcodeZebra.GetInstance.Resume;
  end;
{$ENDIF}
end;

class procedure TProviderBarcode.SetHandler(AHandler: TProc<string>);
begin
{$IFDEF ANDROID}
  case TProviderBarcode.Model of
    scHoneywell: TProviderBarcodeHoneywell.GetInstance.SetHandler(AHandler);
    scZebra    : TProviderBarcodeZebra.GetInstance.SetHandler(AHandler);
  end;
{$ENDIF}
end;

class function TProviderBarcode.GetHandler: TProc<string>;
begin
{$IFDEF ANDROID}
   case TProviderBarcode.Model of
    scHoneywell: Result := TProviderBarcodeHoneywell.GetInstance.GetHandler();
    scZebra    : Result := TProviderBarcodeZebra.GetInstance.GetHandler();
  end;
{$ENDIF}
end;

class procedure TProviderBarcode.SetPausedCallback(AHandler: TProc);
begin
{$IFDEF ANDROID}
   case TProviderBarcode.Model of
    scHoneywell: TProviderBarcodeHoneywell.GetInstance.SetPausedHandler(AHandler);
    scZebra    : TProviderBarcodeZebra.GetInstance.SetPausedHandler(AHandler);
  end;
{$ENDIF}
end;

initialization

{$IFDEF ANDROID}
 if JStringToString(TJBuild.JavaClass.MANUFACTURER).ToUpper.Contains('HONEYWELL') then
   TProviderBarcode.Model := scHoneywell
 else
  TProviderBarcode.Model := scZebra;
{$ENDIF}

end.
